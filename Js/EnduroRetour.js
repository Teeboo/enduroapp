"use strict";
const app = Vue.createApp({
    data() {
        return {
            dossards:[],
            idTicketsSelected: null,
            DossardNumber:'',
            DossardColor:'',
            DossardNumberColor:'',
            participantChosen:null,
        };
    },
    methods: {

        getParticipantsInformation(){
            if(this.idTicketsSelected){
                fetch(`../Database/EnduroRetour/ParticipantInformation.php?idTicket=${this.idTicketsSelected}`)
                    .then(response => response.json())
                    .then(participantArray => {
                        if (Array.isArray(participantArray) && participantArray.length > 0) {
                            this.participantChosen = participantArray[0];
                        } else {
                            this.participantChosen = null;
                        }
                    });
            }
        },

        UpdateDossard(){
            if (this.idTicketsSelected) {
                fetch(`../Database/EnduroRetour/UpdateDossard.php`,{
                    method : 'POST',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded',},
                    body: `idTicket=${this.idTicketsSelected}`
                })
                this.participantChosen.return_date = true;
            }
        },

        UpdateTicket(field,value){
            if (this.idTicketsSelected) {
                fetch(`../Database/EnduroEntry/UpdateTicket.php`,{
                    method : 'POST',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded',},
                    body: `idTicket=${this.idTicketsSelected}&value=${value}&field=${field}`
                })
            }
        },



    },

    computed: {

        filteredDossard() {
            if(this.DossardNumber === '' && this.DossardColor === '' && this.DossardNumberColor === ''){
                return this.dossards
            } else {
                return this.dossards.filter(d => d.number == this.DossardNumber || this.DossardNumber == '')
                    .filter(d => d.background_color === this.DossardColor || this.DossardColor === '')
                    .filter(d => d.color_number === this.DossardNumberColor || this.DossardNumberColor === '');
            }
        }
    },

    watch: {
        idTicketsSelected() {
            this.getParticipantsInformation();

        }
    },

    mounted() {

        fetch("../Database/EnduroRetour/DossardList.php")
            .then(rep => rep.json())
            .then(lst => {this.dossards = lst;});
    },
});
app.mount("#app");