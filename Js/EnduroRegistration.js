"use strict";
const app = Vue.createApp({
    data() {
        return {
            tickets: [],
            idTicketsSelected: null,
            payment:false,
            searchQuery: '',
            ticketChosen:"",
            dossard:{
                number:'',
                background_color:'',
                number_color:''
            },
            dossardAvailaible:true,
        };
    },

    methods: {

        CheckPayment() {

            if (this.idTicketsSelected) {
                fetch(`../Database/EnduroEntry/checkPayment.php?idTicket=${this.idTicketsSelected}`)
                    .then(response => response.json())
                    .then(isPaid => {
                        this.payment = isPaid;
                    });
            } else {
                this.payment = false;
            }
        },

        ModifyPayment() {

            if (this.idTicketsSelected){
                fetch(`../Database/EnduroEntry/modifyPayment.php`, {
                    method: 'POST',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded',},
                    body: `idTicket=${this.idTicketsSelected}&payment=${this.payment}`
                })
                this.payment = !this.payment;
            }
        },

        GetticketChosen() {

            if (this.idTicketsSelected) {
                fetch(`../Database/EnduroEntry/TicketInformation.php?idTicket=${this.idTicketsSelected}`)
                    .then(response => response.json())
                    .then(ticketArray => {
                        if (Array.isArray(ticketArray) && ticketArray.length > 0) {
                            this.ticketChosen = ticketArray[0];
                        } else {
                            this.ticketChosen = null;
                        }
                    });
            } else {
                this.ticketChosen = null;
            }
        },

        GetDossard(){

            if (this.idTicketsSelected) {
                fetch(`../Database/EnduroEntry/DossardInformation.php?idTicket=${this.idTicketsSelected}`)
                    .then(response => response.json())
                    .then(dossardArray => {
                        if (Array.isArray(dossardArray) && dossardArray.length > 0) {
                            this.dossard = dossardArray[0];
                            this.dossardAvailaible=false;
                        } else {
                            this.dossard.number ='';
                            this.dossard.background_color ='';
                            this.dossard.color_number ='';
                            this.dossardAvailaible=true;
                        }
                    });
            } else {
                this.dossard.number ='';
                this.dossard.background_color ='';
                this.dossard.color_number ='';
                this.dossardAvailaible=true;
            }
        },


        UpdateTicket(field,value){

            if (this.idTicketsSelected) {
                fetch(`../Database/EnduroEntry/UpdateTicket.php`,{
                    method : 'POST',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded',},
                    body: `idTicket=${this.idTicketsSelected}&value=${value}&field=${field}`
                })
            }
        },

        UpdateVehicles(field,value,idVehicle){

            if (this.idTicketsSelected) {
                fetch(`../Database/EnduroEntry/UpdateVehicles.php`,{
                    method : 'POST',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded',},
                    body: `idVehicle=${idVehicle}&value=${value}&field=${field}`
                })
            }
        },

        InsertDossard(){

            if (this.idTicketsSelected) {
                fetch(`../Database/EnduroEntry/InsertDossard.php`,{
                    method : 'POST',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded',},
                    body: `idTicket=${this.idTicketsSelected}&number=${this.dossard.number}&background_color=${this.dossard.background_color}&color_number=${this.dossard.color_number}`
                })
                this.dossardAvailaible=false;
            }
        },

        RemoveDossard(){

            if (this.idTicketsSelected) {
                fetch(`../Database/EnduroEntry/RemoveDossard.php`,{
                    method : 'POST',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded',},
                    body: `idTicket=${this.idTicketsSelected}`
                })
                this.dossard.number ='';
                this.dossard.background_color ='';
                this.dossard.color_number ='';
                this.dossardAvailaible=true;
            }
        }
    },

    computed: {

        filteredTickets() {

            if (this.searchQuery.length < 3 || this.searchQuery === '') {
                return this.tickets;
            }else {
                const word = this.searchQuery.split(" ").filter(word => word.length > 0);
                return this.tickets.filter(ticket =>
                    word.every(term =>
                        ticket.ticket_number.toLowerCase().includes(term.toLowerCase()) ||
                        ticket.last_name.toLowerCase().includes(term.toLowerCase()) ||
                        ticket.first_name.toLowerCase().includes(term.toLowerCase())
                    )
                );
            }
        }

    },

    watch: {

        idTicketsSelected() {
            this.CheckPayment();
            this.GetticketChosen();
            this.GetDossard();
        }

    },

    mounted() {

        fetch("../Database/EnduroEntry/listeTicket.php")
            .then(rep => rep.json())
            .then(lst => {this.tickets = lst;});
    },
});
app.mount("#app");