CREATE DATABASE enduro;
USE enduro;


DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
                                        `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(255) DEFAULT NULL,
    `start_date` datetime DEFAULT NULL,
    `end_date` datetime DEFAULT NULL,
    `inscription_date` datetime DEFAULT NULL,
    `vip_date` datetime DEFAULT NULL,
    `description` varchar(255) DEFAULT NULL,
    `information` varchar(255) DEFAULT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
                                        `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    `description` varchar(255) NOT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `participants`;
CREATE TABLE IF NOT EXISTS `participants` (
                                              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `first_name` varchar(255) DEFAULT NULL,
    `last_name` varchar(255) DEFAULT NULL,
    `mail` varchar(255) DEFAULT NULL,
    `street_name` varchar(255) DEFAULT NULL,
    `house_number` varchar(255) DEFAULT NULL,
    `zip_code` varchar(255) DEFAULT NULL,
    `city` varchar(255) DEFAULT NULL,
    `state` varchar(255) DEFAULT NULL,
    `country` varchar(255) DEFAULT NULL,
    `phone_number` varchar(255) DEFAULT NULL,
    `date_of_birth` datetime DEFAULT NULL,
    `information` varchar(255) DEFAULT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=312 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `workers`;
CREATE TABLE IF NOT EXISTS `workers` (
                                         `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `first_name` varchar(255) DEFAULT NULL,
    `last_name` varchar(255) DEFAULT NULL,
    `mail` varchar(255) DEFAULT NULL,
    `phone_number` varchar(255) DEFAULT NULL,
    `information` varchar(255) DEFAULT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE IF NOT EXISTS `vehicles` (
                                          `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `type` varchar(255) DEFAULT NULL,
    `plate_number` varchar(255) DEFAULT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=312 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `ticket_types`;
CREATE TABLE IF NOT EXISTS `ticket_types` (
                                              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(255) DEFAULT NULL,
    `price` decimal(8,2) DEFAULT NULL,
    `event_id` bigint(20) unsigned DEFAULT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `ticket_types_event_id_foreign` (`event_id`),
    CONSTRAINT `ticket_types_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `tickets`;
CREATE TABLE IF NOT EXISTS `tickets` (
                                         `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `ticket_number` varchar(255) DEFAULT NULL,
    `vehicles_id` bigint(20) unsigned DEFAULT NULL,
    `ticket_types_id` bigint(20) unsigned DEFAULT NULL,
    `payment_status` varchar(255) DEFAULT NULL,
    `order_number` varchar(255) DEFAULT NULL,
    `order_date` datetime DEFAULT NULL,
    `meal_tickets` int(11) DEFAULT NULL,
    `comment` varchar(255) DEFAULT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `tickets_vehicles_id_foreign` (`vehicles_id`),
    KEY `tickets_ticket_types_id_foreign` (`ticket_types_id`),
    CONSTRAINT `tickets_ticket_types_id_foreign` FOREIGN KEY (`ticket_types_id`) REFERENCES `ticket_types` (`id`),
    CONSTRAINT `tickets_vehicles_id_foreign` FOREIGN KEY (`vehicles_id`) REFERENCES `vehicles` (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=312 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `dossards`;
CREATE TABLE IF NOT EXISTS `dossards` (
                                          `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `id_ticket` bigint(20) unsigned DEFAULT NULL,
    `number` int(11) DEFAULT NULL,
    `color_number` varchar(255) DEFAULT NULL,
    `background_color` varchar(255) DEFAULT NULL,
    `attribution_date` datetime DEFAULT NULL,
    `return_date` datetime DEFAULT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `dossards_id_ticket_foreign` (`id_ticket`),
    CONSTRAINT `dossards_id_ticket_foreign` FOREIGN KEY (`id_ticket`) REFERENCES `tickets` (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=92152363 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `groups_events`;
CREATE TABLE IF NOT EXISTS `groups_events` (
                                               `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `events_id` bigint(20) unsigned NOT NULL,
    `groups_id` bigint(20) unsigned NOT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `groups_events_events_id_foreign` (`events_id`),
    KEY `groups_events_groups_id_foreign` (`groups_id`),
    CONSTRAINT `groups_events_events_id_foreign` FOREIGN KEY (`events_id`) REFERENCES `events` (`id`) ON DELETE CASCADE,
    CONSTRAINT `groups_events_groups_id_foreign` FOREIGN KEY (`groups_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `participants_tickets`;
CREATE TABLE IF NOT EXISTS `participants_tickets` (
                                                      `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `participants_id` bigint(20) unsigned NOT NULL,
    `tickets_id` bigint(20) unsigned NOT NULL,
    `role_name` varchar(255) DEFAULT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `participants_tickets_participants_id_foreign` (`participants_id`),
    KEY `participants_tickets_tickets_id_foreign` (`tickets_id`),
    CONSTRAINT `participants_tickets_participants_id_foreign` FOREIGN KEY (`participants_id`) REFERENCES `participants` (`id`) ON DELETE CASCADE,
    CONSTRAINT `participants_tickets_tickets_id_foreign` FOREIGN KEY (`tickets_id`) REFERENCES `tickets` (`id`) ON DELETE CASCADE
    ) ENGINE=InnoDB AUTO_INCREMENT=312 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `role_worker_events`;
CREATE TABLE IF NOT EXISTS `role_worker_events` (
                                                    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `workers_id` bigint(20) unsigned NOT NULL,
    `events_id` bigint(20) unsigned NOT NULL,
    `role_name` varchar(255) DEFAULT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `role_worker_events_workers_id_foreign` (`workers_id`),
    KEY `role_worker_events_events_id_foreign` (`events_id`),
    CONSTRAINT `role_worker_events_events_id_foreign` FOREIGN KEY (`events_id`) REFERENCES `events` (`id`) ON DELETE CASCADE,
    CONSTRAINT `role_worker_events_workers_id_foreign` FOREIGN KEY (`workers_id`) REFERENCES `workers` (`id`) ON DELETE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
                                          `id` varchar(255) NOT NULL,
    `user_id` bigint(20) unsigned DEFAULT NULL,
    `ip_address` varchar(45) DEFAULT NULL,
    `user_agent` text DEFAULT NULL,
    `payload` longtext NOT NULL,
    `last_activity` int(11) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `sessions_user_id_index` (`user_id`),
    KEY `sessions_last_activity_index` (`last_activity`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `events` (`name`, `start_date`, `end_date`, `inscription_date`, `vip_date`, `description`, `information`, `created_at`, `updated_at`, `id`)
VALUES
    ('Enduro Championship', '2024-08-01 09:00:00', '2024-08-05 18:00:00', '2024-06-01 09:00:00', '2024-05-01 09:00:00', 'Annual Enduro championship.', 'Bring your best!', NOW(), NOW(),1);


INSERT INTO `groups` (`name`, `description`, `created_at`, `updated_at`)
VALUES
    ('Group A', 'First group for the event.', NOW(), NOW()),
    ('Group B', 'Second group for the event.', NOW(), NOW());


INSERT INTO `participants` (`first_name`, `last_name`, `mail`, `street_name`, `house_number`, `zip_code`, `city`, `state`, `country`, `phone_number`, `date_of_birth`, `information`, `created_at`, `updated_at`,`id`)
VALUES
    ('John', 'Doe', 'john.doe@example.com', 'Main St', '123', '12345', 'Springfield', 'IL', 'USA', '123-456-7890', '1980-01-01', 'N/A', NOW(), NOW(),1),
    ('Jane', 'Smith', 'jane.smith@example.com', 'Elm St', '456', '67890', 'Metropolis', 'NY', 'USA', '987-654-3210', '1985-05-15', 'N/A', NOW(), NOW(),2);


INSERT INTO `workers` (`first_name`, `last_name`, `mail`, `phone_number`, `information`, `created_at`, `updated_at`)
VALUES
    ('Alice', 'Johnson', 'alice.johnson@example.com', '555-1234', 'Team Leader', NOW(), NOW()),
    ('Bob', 'Williams', 'bob.williams@example.com', '555-5678', 'Support Staff', NOW(), NOW());


INSERT INTO `vehicles` (`type`, `plate_number`, `created_at`, `updated_at`, `id`)
VALUES
    ('Motorbike', 'ABC-123', NOW(), NOW(),1),
    ('Quad', 'XYZ-789', NOW(), NOW(),2);


INSERT INTO `ticket_types` (`name`, `price`, `event_id`, `created_at`, `updated_at`,`id`)
VALUES
    ('Regular Ticket', 100.00, 1, NOW(), NOW(),1),
    ('VIP Ticket', 250.00, 1, NOW(), NOW(),2);


INSERT INTO `tickets` (`ticket_number`, `vehicles_id`, `ticket_types_id`, `payment_status`, `order_number`, `order_date`, `meal_tickets`, `comment`, `created_at`, `updated_at`, `id`)
VALUES
    ('TICK123', 1, 1, 'Paid', 'ORD123', NOW(), 2, 'N/A', NOW(), NOW(),1),
    ('TICK456', 2, 2, 'Paid', 'ORD456', NOW(), 3, 'N/A', NOW(), NOW(),2);

INSERT INTO `groups_events` (`events_id`, `groups_id`, `created_at`, `updated_at`)
VALUES
    (1, 1, NOW(), NOW()),
    (1, 2, NOW(), NOW());

INSERT INTO `participants_tickets` (`participants_id`, `tickets_id`, `role_name`, `created_at`, `updated_at`)
VALUES
    (1, 1, 'Rider', NOW(), NOW()),
    (2, 2, 'Rider', NOW(), NOW());

INSERT INTO `role_worker_events` (`workers_id`, `events_id`, `role_name`, `created_at`, `updated_at`)
VALUES
    (1, 1, 'Organizer', NOW(), NOW()),
    (2, 1, 'Helper', NOW(), NOW());