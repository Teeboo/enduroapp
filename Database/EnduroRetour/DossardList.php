<?php

include('../dbconnection.php');

$statement = $pdo->prepare(
    "SELECT id as idDossard, id_ticket as IdTicket, number,color_number,background_color
                FROM dossards;"
);

$statement->execute();
$result = $statement->fetchAll(PDO::FETCH_ASSOC);

$json = json_encode($result);
echo $json;