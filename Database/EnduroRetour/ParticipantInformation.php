<?php
include('../dbconnection.php');

$idTicket = $_GET['idTicket'];


if($idTicket){

    $statement = $pdo->prepare(
        "SELECT p.last_name, p.first_name,p.phone_number,p.information,t.ticket_number,t.meal_tickets,d.return_date
                    FROM tickets as t 
                    INNER JOIN participants_tickets as pt on t.id = pt.tickets_id
                    INNER JOIN participants as p on p.id = pt.participants_id
                    INNER JOIN dossards as d on d.id_ticket = t.id
                        WHERE t.id = ?;"
    );

    $statement->execute([$idTicket]);
    $result = $statement->fetchAll(PDO::FETCH_ASSOC);

    if($result !=null && count($result)>0){

        $json = json_encode($result);
        echo $json;

    } else {
        echo json_encode('');
    }
}