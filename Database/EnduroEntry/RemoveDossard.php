<?php
include('../dbconnection.php');

$idTicket =$_POST['idTicket'];

if ($idTicket) {

    $statement = $pdo->prepare("DELETE FROM dossards WHERE id_ticket = ?");
    $statement->execute([$idTicket]);

}