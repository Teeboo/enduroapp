<?php
include('../dbconnection.php');

$idTicket = $_GET['idTicket'];

if ($idTicket) {

    $statement = $pdo->prepare("SELECT payment_status FROM tickets WHERE id = :idTicket");
    $statement->bindParam(':idTicket', $idTicket, PDO::PARAM_INT);
    $statement->execute();
    $result = $statement->fetch(PDO::FETCH_ASSOC);

    if ($result && $result['payment_status'] !== null) {
        echo json_encode(true);
    } else {
        echo json_encode(false);
    }
} else {
    echo json_encode(["error" => "Aucun Ticket"]);
}
