<?php

include('../dbconnection.php');

$statement = $pdo->prepare(
    "SELECT tickets.id as idTicket,tickets.ticket_number,p.last_name,p.first_name,p.id as idParticipant FROM tickets
                INNER JOIN participants_tickets as pt on tickets.id = pt.tickets_id
                INNER JOIN participants as p on pt.participants_id = p.id
            ;"
);

$statement->execute();

$result = $statement->fetchAll(PDO::FETCH_ASSOC);

$json = json_encode($result);
echo $json;

