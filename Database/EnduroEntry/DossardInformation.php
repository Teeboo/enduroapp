<?php
include('../dbconnection.php');

$idTicket = $_GET['idTicket'];

if($idTicket){

    $statement = $pdo->prepare(
        "SELECT d.number,d.color_number,d.background_color FROM dossards as d
                    INNER JOIN tickets as t ON d.id_ticket = t.id
                        WHERE d.id_ticket = ?;"
    );

    $statement->execute([$idTicket]);
    $result = $statement->fetchAll(PDO::FETCH_ASSOC);

    if($result !=null && count($result)>0){

        $json = json_encode($result);
        echo $json;

    } else {
        echo json_encode('');
    }

}

