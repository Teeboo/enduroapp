<?php
include('../dbconnection.php');

$idTicket =$_POST['idTicket'];
$payment = $_POST['payment'];

if ($idTicket) {

    if($payment == "false"){
        $payment = "Paid";
    } else {
        $payment = NULL;
    }

    $statement = $pdo->prepare("UPDATE tickets SET payment_status = ? WHERE id = ?");
    $statement->execute([$payment, $idTicket]);
}
