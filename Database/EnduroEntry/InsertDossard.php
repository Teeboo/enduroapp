<?php
include('../dbconnection.php');

$idTicket =$_POST['idTicket'];
$number = $_POST['number'];
$background_color = $_POST['background_color'];
$color_number = $_POST['color_number'];


if ($idTicket && $number && $background_color && $color_number) {

    $statement = $pdo->prepare("INSERT INTO dossards (number, background_color, color_number,id_ticket) VALUES(?,?,?,?)");
    $statement->execute([$number,$background_color,$color_number, $idTicket]);

}