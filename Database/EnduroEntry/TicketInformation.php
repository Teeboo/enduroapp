<?php
include('../dbconnection.php');

$idTicket = $_GET['idTicket'];



$statement = $pdo->prepare(
    "SELECT tickets.id as idTicket,p.phone_number,p.information,p.last_name,p.first_name,tickets.ticket_number ,p.id as idParticipant,v.type,v.plate_number,v.id as idVehicles
                FROM tickets
                INNER JOIN participants_tickets as pt on tickets.id = pt.tickets_id
                INNER JOIN participants as p on pt.participants_id = p.id
                INNER JOIN vehicles as v on v.id = tickets.ticket_types_id
                    WHERE tickets.id = ?;
        ;"
);

$statement->execute([$idTicket]);

$result = $statement->fetchAll(PDO::FETCH_ASSOC);

$json = json_encode($result);
echo $json;

